package com.jaroof.dmn;

import com.jaroof.dmn.service.CliXmlDomService;
import picocli.CommandLine;
import java.io.File;

@CommandLine.Command(name = "DMN-CLI-Tool",
        description = "This is a CLI tool for appending large numbers of rows to a DMN decision table (specifically, " +
                "relation tables). The column count, type, and order of the CSV file should match the contents of the " +
                "decision table.\r\n")
public class CliCommand implements Runnable {

    @CommandLine.Option(names = { "-f", "--file" }, required = true, paramLabel = "/path/to/dmn-input-file.dmn",
            description = "A DMN input file")
    private File dmnInputFile;

    @CommandLine.Option(names = { "-o", "--output" }, required = true, paramLabel = "/path/to/dmn-output-file.dmn",
            description = "This is a new DMN file that will be created from this process")
    private File dmnOutputFile;

    @CommandLine.Option(names = { "-c", "--csv" }, required = true, paramLabel = "/path/to/csv-file.csv",
            description = "A csv file containing Decision table data")
    private File csvFile;

    @CommandLine.Option(names = { "-d", "--delimiter" }, required = false, paramLabel = ",", defaultValue = ",",
            description = "The column delimiter for the CSV file.")
    private String delimiter;

    @CommandLine.Option(names = { "-t", "--table" }, required = true, paramLabel = "Decision-table-name",
            description = "The decision table name to append with CSV values")
    private String decisionTableName;

    @CommandLine.Option(names = { "-s", "--skip" }, required = false, paramLabel = "true/false",
            description = "Skip the first line of the CSV file")
    private boolean skipFirstLine;

    @CommandLine.Option(names = { "-h", "-?", "--help" }, usageHelp = true, description = "display a help message")
    private boolean helpRequested;

    //private CliCamundaService cliCamundaService;
    private CliXmlDomService cliXmlDomService;

    public CliCommand(CliXmlDomService cliXmlDomService) {
        this.cliXmlDomService = cliXmlDomService;
    }

    @Override
    public void run() {
        System.out.println("\r\nStarting DMN-CLI-Tool!  Restart with '-h' for help");
        System.out.println("Checking the DMN and CSV files...");

        if (!dmnInputFile.isFile() || !csvFile.isFile()) {
            if (!dmnInputFile.isFile()) {
                System.out.println("ERROR! " + dmnInputFile.getName() + " is not a DMN file. Verify the file path");
            }
            if (!csvFile.isFile()) {
                System.out.println("ERROR! " + csvFile.getName() + " is not a CSV file. Verify the file path");
            }
            System.exit(1);
        }
        System.out.println("Verified the files...");
        cliXmlDomService.appendDecisionTable(dmnInputFile, dmnOutputFile, csvFile, delimiter, decisionTableName, skipFirstLine);
    }

}
