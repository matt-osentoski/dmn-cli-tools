package com.jaroof.dmn.model;

import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.Data;

@Data
@RegisterForReflection //Required to let quarkus use lombok
public class DecisionColumnTypeDTO {
    private String name;
    private String id;
    private String typeRef;
    private String inputOutput;

}
