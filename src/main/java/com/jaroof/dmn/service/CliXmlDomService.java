package com.jaroof.dmn.service;

import com.jaroof.dmn.model.DecisionColumnTypeDTO;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.traversal.DocumentTraversal;
import org.xml.sax.SAXException;

import javax.enterprise.context.ApplicationScoped;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@ApplicationScoped
public class CliXmlDomService implements CliService {

    public void appendDecisionTable(File dmnInputFile, File dmnOutputFile, File csvFile, String delimiter,
                                    String decisionTable, boolean skipFirstLine) {
        Document document = this.getDocumentFromFile(dmnInputFile);
        XPath xPath = XPathFactory.newInstance().newXPath();
        appendRelationTable(xPath, csvFile, delimiter, skipFirstLine, decisionTable, document);
        appendDecisionTable(xPath, csvFile, delimiter, skipFirstLine, decisionTable, document);

        // Save output to a new DMN file using the updated model.
        DOMSource source = new DOMSource(document);
        StreamResult file = new StreamResult(dmnOutputFile);
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transf = null;
        try {
            transf = transformerFactory.newTransformer();
//            transf.setOutputProperty(OutputKeys.INDENT, "yes");
//            transf.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            transf.transform(source, file);
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
            System.exit(1);
        } catch (TransformerException e) {
            e.printStackTrace();
            System.exit(1);
        }
        System.out.println("Saving file: " + dmnOutputFile);
        System.exit(1);
    }

    private void appendDecisionTable(XPath xPath, File csvFile, String delimiter, boolean skipFirstLine,
                                     String decisionTable, Document document) {
        try {
            List<CSVRecord> csvRecords = getCsvRecords(csvFile, delimiter, skipFirstLine);

            NodeList nodes = (NodeList)xPath.evaluate("//*[local-name()='decision']",
                    document, XPathConstants.NODESET);
            int decisionCount = nodes.getLength();
            System.out.println("Found " + decisionCount + " decision nodes");

            for (int i = 0; i < nodes.getLength(); ++i) {
                Element decisionNode = (Element) nodes.item(i);
                if (decisionTable.equals(decisionNode.getAttribute("name"))) {
                    NodeList decisionTableNodes = (NodeList)xPath.evaluate(".//*[local-name()='decisionTable']",
                            decisionNode, XPathConstants.NODESET);
                    int decisionTableCount = decisionTableNodes.getLength();
                    System.out.println("Found " + decisionTableCount + " decisionTable nodes");
                    if (decisionTableCount > 0) {
                        Node decisionTableNode = decisionTableNodes.item(0);
                        List<DecisionColumnTypeDTO> columnTypes = gatherDecisionTableColumnInfo(decisionNode, decisionTableNode, xPath);
                        addRowsToDecisionTable(document, decisionTableNode, columnTypes, csvRecords, xPath);
                    }
                    break;
                } else {
                    System.out.println("No Decision Table was found, skipping the decisionTable processor...");
                }
            }

        } catch (XPathExpressionException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("ERROR: There was a problem reading the CSV file: " + e.getMessage());
            System.exit(1);
        }

    }

    protected void appendRelationTable(XPath xPath, File csvFile, String delimiter, boolean skipFirstLine, String decisionTable,
                                     Document document) {
        try {
            List<CSVRecord> csvRecords = getCsvRecords(csvFile, delimiter, skipFirstLine);

            NodeList nodes = (NodeList)xPath.evaluate("//*[local-name()='decision']",
                    document, XPathConstants.NODESET);
            int decisionCount = nodes.getLength();
            System.out.println("Found " + decisionCount + " decision nodes");

            for (int i = 0; i < nodes.getLength(); ++i) {
                Element e = (Element) nodes.item(i);
                if (decisionTable.equals(e.getAttribute("name"))) {
                    NodeList relationNodes = (NodeList)xPath.evaluate(".//*[local-name()='relation']",
                            e, XPathConstants.NODESET);
                    int relationsCount = relationNodes.getLength();
                    System.out.println("Found " + relationsCount + " relation nodes");
                    if (relationsCount > 0) {
                        Node relation = relationNodes.item(0);
                        List<DecisionColumnTypeDTO> columnTypes = gatherRelationColumnInfo(relation, xPath);
                        addRowsToRelation(document, relation, columnTypes, csvRecords, xPath);
                    }
                    break;
                } else {
                    System.out.println("No Relation Table was found, skipping the relation processor...");
                }
            }

        } catch (XPathExpressionException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("ERROR: There was a problem reading the CSV file: " + e.getMessage());
            System.exit(1);
        }
    }

    protected void addRowsToDecisionTable(Document document, Node decisionTable, List<DecisionColumnTypeDTO> columnTypes,
                                          List<CSVRecord> csvRecords, XPath xPath) throws XPathExpressionException {
        for (CSVRecord csvRecord : csvRecords) {
            Element ruleNode = document.createElement("dmn:rule");
            ruleNode.setAttribute("id", getUUID());

            for (int x=0; x<csvRecord.size(); x++) {
                Element textNode = document.createElement("dmn:text");
                if ("string".equals(columnTypes.get(x).getTypeRef())) {
                    if (!csvRecord.get(x).toString().startsWith("\"") && !csvRecord.get(x).toString().endsWith("\"")) {
                        textNode.setTextContent("\"" + csvRecord.get(x).toString() + "\"");
                    } else {
                        textNode.setTextContent(csvRecord.get(x).toString());
                    }
                } else {
                    textNode.setTextContent(csvRecord.get(x).toString());
                }
                if ("input".equals(columnTypes.get(x).getInputOutput())) {
                    Element inputEntryNode = document.createElement("dmn:inputEntry");
                    inputEntryNode.setAttribute("id", getUUID());
                    inputEntryNode.appendChild(textNode);
                    ruleNode.appendChild(inputEntryNode);
                } else if ("output".equals(columnTypes.get(x).getInputOutput())) {
                    Element inputEntryNode = document.createElement("dmn:outputEntry");
                    inputEntryNode.setAttribute("id", getUUID());
                    inputEntryNode.appendChild(textNode);
                    ruleNode.appendChild(inputEntryNode);
                }

            }

            Element annotationTextNode = document.createElement("dmn:text");
            Element annotationNode = document.createElement("dmn:annotationEntry");
            annotationNode.setAttribute("id", getUUID());
            annotationNode.appendChild(annotationTextNode);
            ruleNode.appendChild(annotationNode);

            decisionTable.appendChild(ruleNode);
        }
    }

    protected void addRowsToRelation(Document document, Node relation, List<DecisionColumnTypeDTO> columnTypes,
                                   List<CSVRecord> csvRecords, XPath xPath) throws XPathExpressionException {

        for (CSVRecord csvRecord : csvRecords) {
            Element rowNode = document.createElement("dmn:row");
            rowNode.setAttribute("id", getUUID());

            for (int x=0; x<csvRecord.size(); x++) {
                Element textNode = document.createElement("dmn:text");
                if ("string".equals(columnTypes.get(x).getTypeRef())) {
                    if (!csvRecord.get(x).toString().startsWith("\"") && !csvRecord.get(x).toString().endsWith("\"")) {
                        textNode.setTextContent("\"" + csvRecord.get(x).toString() + "\"");
                    } else {
                        textNode.setTextContent(csvRecord.get(x).toString());
                    }
                } else {
                    textNode.setTextContent(csvRecord.get(x).toString());
                }
                Element literalExpressionNode = document.createElement("dmn:literalExpression");
                literalExpressionNode.setAttribute("id", getUUID());
                literalExpressionNode.appendChild(textNode);
                rowNode.appendChild(literalExpressionNode);
            }

            relation.appendChild(rowNode);
        }
    }

    private List<DecisionColumnTypeDTO> gatherDecisionTableColumnInfo(Element decisionNode, Node decisionTable, XPath xPath)
            throws XPathExpressionException {

        NodeList inputsNodes = (NodeList)xPath.evaluate(".//*[local-name()='inputExpression']",
                decisionTable, XPathConstants.NODESET);

        NodeList outputNodes = (NodeList)xPath.evaluate(".//*[local-name()='output']",
                decisionTable, XPathConstants.NODESET);

        // We'll first check all the column types for the table and store them in this
        // array list, for use later by the CSV processor
        List<DecisionColumnTypeDTO> columnTypes = new ArrayList<>();

        for (int i = 0; i < inputsNodes.getLength(); i++) {
            Element e = (Element) inputsNodes.item(i);
            DecisionColumnTypeDTO columnType = new DecisionColumnTypeDTO();
            columnType.setTypeRef(e.getAttribute("typeRef"));
            columnType.setId(e.getAttribute("id"));
            columnType.setInputOutput("input");
            Node inputNameNode = e.getElementsByTagName("dmn:text").item(0);
            columnType.setName(inputNameNode.getTextContent());
            columnTypes.add(columnType);
        }

        // Sometimes an output is only defined at the DecisionTable level, for single outputs
        Element eTest = (Element) outputNodes.item(0);
        if (eTest.hasAttribute("name")) {
            for (int i = 0; i < outputNodes.getLength(); i++) {
                Element e = (Element) outputNodes.item(i);
                DecisionColumnTypeDTO columnType = new DecisionColumnTypeDTO();
                columnType.setTypeRef(e.getAttribute("typeRef"));
                columnType.setId(e.getAttribute("id"));
                columnType.setInputOutput("output");
                columnType.setName(e.getAttribute("name"));
                columnTypes.add(columnType);
            }
        } else {
            NodeList variableNodes = (NodeList)xPath.evaluate(".//*[local-name()='variable']",
                    decisionNode, XPathConstants.NODESET);

            Element e = (Element) variableNodes.item(0);
            DecisionColumnTypeDTO columnType = new DecisionColumnTypeDTO();
            columnType.setTypeRef(e.getAttribute("typeRef"));
            columnType.setId(e.getAttribute("id"));
            columnType.setInputOutput("output");
            columnType.setName(e.getAttribute("name"));
            columnTypes.add(columnType);
        }

        return columnTypes;
    }

    protected List<DecisionColumnTypeDTO> gatherRelationColumnInfo(Node relation, XPath xPath) throws XPathExpressionException {
        NodeList columnNodes = (NodeList)xPath.evaluate(".//*[local-name()='column']",
                relation, XPathConstants.NODESET);

        // We'll first check all the column types for the table and store them in this
        // array list, for use later by the CSV processor
        List<DecisionColumnTypeDTO> columnTypes = new ArrayList<>();

        for (int i = 0; i < columnNodes.getLength(); i++) {
            Element e = (Element) columnNodes.item(i);
            DecisionColumnTypeDTO columnType = new DecisionColumnTypeDTO();
            columnType.setName(e.getAttribute("name"));
            columnType.setId(e.getAttribute("id"));
            columnType.setTypeRef(e.getAttribute("typeRef"));
            columnTypes.add(columnType);
        }

        return columnTypes;
    }

    protected List<CSVRecord> getCsvRecords(File csvFile, String delimiter, boolean skipFirstLine) throws IOException {
        FileReader filereader = new FileReader(csvFile);
        CSVFormat csvFormat = null;

        if (skipFirstLine) {
            csvFormat = CSVFormat.RFC4180
                    .withFirstRecordAsHeader()
                    .withDelimiter(delimiter.charAt(0))
                    .withSkipHeaderRecord(true)
                    .withEscape('\\');
        } else {
            csvFormat = CSVFormat.RFC4180
                    .withFirstRecordAsHeader()
                    .withDelimiter(delimiter.charAt(0))
                    .withEscape('\\');
        }
        CSVParser parser = new CSVParser(filereader, csvFormat);
        List<CSVRecord> records = parser.getRecords();
        return records;
    }

    protected Document getDocumentFromFile(File dmnFile) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        Document document = null;
        try {
            DocumentBuilder loader = factory.newDocumentBuilder();
            document = loader.parse(dmnFile.getAbsolutePath());
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return document;
    }

    protected String getUUID() {
        return "_"+UUID.randomUUID().toString().toUpperCase();
    }
}
