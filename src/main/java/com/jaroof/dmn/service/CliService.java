package com.jaroof.dmn.service;

import java.io.File;

public interface CliService {
    void appendDecisionTable(File dmnInputFile, File dmnOutputFile, File csvFile, String delimiter,
                             String decisionTable, boolean skipFirstLine);
}
