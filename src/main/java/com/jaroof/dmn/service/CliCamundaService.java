package com.jaroof.dmn.service;

import com.jaroof.dmn.model.DecisionColumnTypeDTO;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.camunda.bpm.model.dmn.Dmn;
import org.camunda.bpm.model.dmn.DmnModelInstance;

import org.camunda.bpm.model.dmn.instance.*;
import javax.enterprise.context.ApplicationScoped;
import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

/**
 * NOTE: This service is not used!
 *
 * I'm having trouble getting the Camunda Model API to work with Kie DMN models.
 * For the time being, I'll use Java DOM XML parsing directly via the CliXmlDomService
 * I'm keeping this file around for historical purposes
 */
@ApplicationScoped
public class CliCamundaService implements CliService {

    public void appendDecisionTable(File dmnInputFile, File dmnOutputFile, File csvFile, String delimiter,
                                    String decisionTable, boolean skipFirstLine) {
        DmnModelInstance modelInstance = this.getDmnFromFile(dmnInputFile);
        Decision decision = modelInstance.getModelElementById(decisionTable);

        //relation table
        String typeName = decision.getElementType().getTypeName();
        //Todo add logic to look for relation and dataTable types
        appendRelationTable(csvFile, delimiter, skipFirstLine, modelInstance, decision);

        // Save output to a new DMN file using the updated model.
        Dmn.validateModel(modelInstance);
        Dmn.writeModelToFile(dmnOutputFile, modelInstance);
    }

    private void appendRelationTable(File csvFile, String delimiter, boolean skipFirstLine, DmnModelInstance modelInstance, Decision decision) {
        Collection<Relation> relations = decision.getChildElementsByType(Relation.class);
        for (Relation relation : relations) {
            List<DecisionColumnTypeDTO> columnTypes = gatherRelationColumnInfo(relation);

            List<CSVRecord> csvRecords = null;
            try {
                csvRecords = getCsvRecords(csvFile, delimiter, skipFirstLine);
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("ERROR: There was a problem reading the CSV file: " + e.getMessage());
                System.exit(1);
            }

            addRowsToRelation(modelInstance, relation, columnTypes, csvRecords);
            break; // Just grab the first relation
        }
    }

    private void addRowsToRelation(DmnModelInstance modelInstance, Relation relation, List<DecisionColumnTypeDTO> columnTypes, List<CSVRecord> csvRecords) {
        for (CSVRecord csvRecord : csvRecords) {
            Row row = modelInstance.newInstance(Row.class);
            row.setId("_"+UUID.randomUUID().toString());

            for (int x=0; x<csvRecord.size(); x++) {
                Text text = modelInstance.newInstance(Text.class);
                if ("string".equals(columnTypes.get(0).getTypeRef())) {
                    text.setTextContent("\"" + csvRecord.get(x).toString() + "\"");
                } else {
                    text.setTextContent(csvRecord.get(x).toString());
                }
                LiteralExpression literalExpression = modelInstance.newInstance(LiteralExpression.class);
                literalExpression.setId("_" + UUID.randomUUID().toString());
                literalExpression.setText(text);
                row.addChildElement(literalExpression);
            }

            relation.addChildElement(row);
        }
    }

    protected List<DecisionColumnTypeDTO> gatherRelationColumnInfo(Relation relation) {
        Collection<Column> columns = relation.getColumns();

        // We'll first check all the column types for the table and store them in this
        // array list, for use later by the CSV processor
        List<DecisionColumnTypeDTO> columnTypes = new ArrayList<>();
        for(Column column : columns) {
            DecisionColumnTypeDTO columnType = new DecisionColumnTypeDTO();
            columnType.setName(column.getName());
            columnType.setId(column.getId());
            columnType.setTypeRef(column.getTypeRef());
            columnTypes.add(columnType);
        }
        return columnTypes;
    }

    protected List<CSVRecord> getCsvRecords(File csvFile, String delimiter, boolean skipFirstLine) throws IOException {
        FileReader filereader = new FileReader(csvFile);
        CSVFormat csvFormat = null;
        if (skipFirstLine) {
            csvFormat = CSVFormat.RFC4180.withDelimiter(delimiter.charAt(0)).withFirstRecordAsHeader();
        } else {
            csvFormat = CSVFormat.RFC4180.withDelimiter(delimiter.charAt(0));
        }
        CSVParser parser = new CSVParser(filereader, csvFormat);
        List<CSVRecord> records = parser.getRecords();
        return records;
    }

    protected DmnModelInstance getDmnFromFile(File dmnFile) {
        DmnModelInstance modelInstance = Dmn.readModelFromFile(dmnFile);
        return modelInstance;
    }
}
