# dmn-cli-tools 
This project is a simple CLI tool that allows you to append entries into
DMN DecisionTables and Relation Tables from CSV files.  This tool has been specifically built
toward [Kogito](https://docs.jboss.org/kogito/release/latest/html_single/#chap-kogito-using-dmn-models) DMN files.

## Instructions
### Create a DMN file using the VSCode plugin:

[https://marketplace.visualstudio.com/items?itemName=kie-group.vscode-extension-kogito-bundle](https://marketplace.visualstudio.com/items?itemName=kie-group.vscode-extension-kogito-bundle)

### Create empty DecisionTables
Create a Decision table with columns (input/output) defined. To avoid errors make sure to
define the output types explicitly.

You can also create a Relation Table instead of a Decision table.  The CLI will automatically know
which table to process.

Make a note of the DecisionTable name, this will be used in the command line args.

### Create a CSV File
The CSV file should match the number of columns and be in the correct order. For example,
if you have a DecisionTable with three input fields and two output fields, then the CSV
would have five columns with the input columns first, followed by the output columns. Again, the order
must match.

### Run the CLI tool and inspect the output file
The CLI tool will create a new DMN file with the rows appended to the DecisionTable that you
specified.  Verify that this DMN file looks correct and will execute in the Kogito runtime.



## Maven Build
```
mvn clean package
```

## Running the CLI
```
cd target
java -jar dmn-cli-tools-1.0-SNAPSHOT-runner.jar --help
```

## Example with CLI arguments
```
java -jar dmn-cli-tools-1.0-SNAPSHOT-runner.jar \
    --csv=/sample-files/test-relation.csv \
    --table=LifeExpectancyTable \
    --file=sample-files/test.dmn \
    --output=sample-files/my-new-test.dmn
```


## Debugging in IntelliJ
To Debug in IntelliJ, use the instructions from this StackOverflow Answer:

[https://stackoverflow.com/questions/21114066/attach-intellij-idea-debugger-to-a-running-java-process](https://stackoverflow.com/questions/21114066/attach-intellij-idea-debugger-to-a-running-java-process)

- Run -> Edit Configurations...
- Click the "+" in the upper left
- Select the "Remote" option in the left-most pane
- Choose a name (I named mine "remote-debugging")
- Click "OK" to save:

```
mvn quarkus:dev -Dsuspend=true "-Dquarkus.args=-c=/tmp/test-relation.csv -t=LifeExpectancyTable -f=/tmp/test.dmn -o=/tmp/new-test.dmn" -f pom.xml
```

Run the application normally, then in IntelliJ debug using the remote configuration you setup above.